FROM resin/rpi-raspbian:stretch
ENV INITSYSTEM o

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y mosquitto

VOLUME ["/etc/mosquitto", "/var/lib/mosquitto", "/var/log/mosquitto"]

COPY mosquitto.conf /etc/mosquitto/

EXPOSE 1883

CMD ["mosquitto"]

